ENEMY = 2
PLAYER = 1


def do_turn(pw):
	#Try to win:
	num_of_soldiers = 0
	num_of_soldiers_enemy = 0
	if(len(pw.my_planets()) == 0):
		return
	
	for planet in pw.my_planets():
		num_of_soldiers += planet.num_ships()
	for planet in pw.my_planets():
		num_of_soldiers_enemy += planet.num_ships()
		
	if len(pw.enemy_planets()) == 1 and num_of_soldiers > num_of_soldiers_enemy:
		dest = pw.enemy_planets()[0]
		for planet in pw.my_planets():
			source = planet
			num_ships = source.num_ships() * 0.9
			pw.issue_order(source, dest, num_ships)
	else:
		dest = pw.enemy_planets()[0]
		source = pw.my_planets()[0]
		num_ships = source.num_ships()
	